import RPi.GPIO as GPIO
import time
import sys
from dicoMorse import d

Pin = int(sys.argv[1])
Temps = float(sys.argv[2])

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(Pin, GPIO.OUT)

tempsPoint = Temps
tempsBarre = 3*tempsPoint
tempsPause = 2*tempsPoint


def Point():

        GPIO.output(Pin,GPIO.HIGH)
        time.sleep(tempsPoint)
        GPIO.output(Pin,GPIO.LOW)
        time.sleep(tempsPoint)

def Barre():
        
	GPIO.output(Pin,GPIO.HIGH)
	time.sleep(tempsBarre)
	GPIO.output(Pin,GPIO.LOW)
	time.sleep(tempsPoint)

def affMorse(lettre):

                for i in range(len(lettre)):
                        if lettre[i] == '.':

                                Point()

                        elif lettre[i] == '-':
                                
                                Barre()
                        else:
                                time.sleep(tempsPoint)

                time.sleep(tempsPause)

def main():

        try:
                Sortie = 'o'
                while Sortie != 'N':
                        
                        mot = input("Entrez un mot à traduire en Morse : ")
                        mot = mot.lower()
                        
                        for i in range(len(mot)):
                                
                                a = mot[i]
                                
                                if a in d:
                                        
                                        affMorse(d[a])
                                        
                        Sortie = input ("Voulez-vous entrer un autre message?(O/N): ")
                        Sortie = Sortie.upper()

        except:
                GPIO.output(Pin, GPIO.LOW)
main()
